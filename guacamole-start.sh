#!/bin/bash

# Author: sebcworks
# URL   : https://bitbucket.org/sebcworks/ 
#         https://hub.docker.com/r/sebcworks/
# Date  : 2017-09-24
# Ver.  : 0.1.0
# Hist. : 0.1.0 - Initial version

set -e

# Vars
INITDIR="/init"
INITFLAG="$INITDIR/.guacamole_init_ok"
DBTYPE="mysql"

# General purpose functions

log () {
    if [ -z "$1" ]; then
	return 1
    fi
    echo "[`date "+%Y-%m-%dT%H:%M:%S"`] $1"
}


# Create database init script, must provide database type (mysql or postgres)
init () {
    if [ -z "$DBTYPE" ]; then
	log "ERROR: Have to specify database type (mysql or postgres)"
	return 1
    fi
    option="--mysql"
    sqlcommand=""
    sqlfile="$INITDIR/initdb.sql"
    case "$DBTYPE" in
	postgres|pg|postgresql)
	    option="--postgres"
	    sqlcommand="env PGPASSWORD=$POSTGRES_PASSWORD psql -h postgres -U $POSTGRES_USER -d $POSTGRES_DATABASE -f $sqlfile"
            # Check that database is alive and waiting
	    pgtries=5
	    pgwait=5

	    until env PGPASSWORD=$POSTGRES_PASSWORD psql -h postgres -U $POSTGRES_USER -d $POSTGRES_DATABASE -c "select 1" > /dev/null 2>&1 || [ $pgtries -eq 0 ]; do
		pgtries=`expr $pgtries - 1`
		log "Postgres server out of reach, wait $pgwait sec before retying ($pgtries left)"
		sleep $pgwait
	    done
	    ;;
	mysql)
	    option="--mysql"
	    sqlcommand="mysql -h mysql -u $MYSQL_USER -p $MYSQL_PASSWORD -D $MYSQL_DATABASE < $sqlfile"
	    ;;
	*)
	    log "ERROR: Unknown database type: $DBTYPE"
	    return 2
	    ;;
    esac
    
    log "Start database initialisation"
    # Create SQL init script
    /opt/guacamole/bin/initdb.sh $option > "$sqlfile"

    # Use the init script
    $sqlcommand
    
    echo "`date "+%Y-%m%dT%H:%M:%S"`" > "$INITFLAG"
    log "Database initialised successfully."
    return 0
}

# Do initial checks on directories and set db type
check () {
    # First, set db type
    if [ -n "$POSTGRES_USER" ]; then
	DBTYPE="postgres"
    else
	DBTYPE="mysql"
    fi
    log "DEBUG: Set DBTYPE as $DBTYPE"
    
    # Check if env vars exists for the selected db type
    if [ "$DBTYPE" = "postgres" ] && [ -z "$POSTGRES_DATABASE" -o -z "$POSTGRES_PASSWORD" -o -z "$POSTGRES_USER" ]; then
	log "ERROR: Missing postgresql environment variables"
	return 1
    elif [ "$DBTYPE" = "mysql" ] && [ -z "$MYSQL_DATABASE" -o -z "$MYSQL_PASSWORD" -o -z "$MYSQL_USER" ]; then
	log "ERROR: Missing mysql environment variables"
	return 1
    fi

    if [ -d "$INITDIR" ]; then
	log "Missing init directory ($INITDIR), creating it..."
	mkdir -p "$INITDIR"
	chmod 750 "$INITDIR"
	log "directory successfully created."
    fi
}

# 1st, check if everything is ok
if ! check; then
    log "ERROR: Can't start guacamole, please check reported errors"
    exit 1
fi

# 2nd, do init if necessary
if [ ! -f "$INITFLAG" ]; then
    init
fi

# 3rd and last, finaly do the launch
/opt/guacamole/bin/start.sh
